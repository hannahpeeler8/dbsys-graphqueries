 @0    2..53   line_comment                // Q18. How many persons have a given number of posts
 @1   56..148  block_comment               /*\n  :param {\n    date: 20110722000000000,\n    lengthThreshold: 20,\n    languages: ['ar']\n  }\n*/
 @2  151..554  statement                   body=@3
 @3  151..554  > query                     clauses=[@4, @10, @49, @57]
 @4  151..173  > > MATCH                   pattern=@5
 @5  157..172  > > > pattern               paths=[@6]
 @6  157..172  > > > > pattern path        (@7)
 @7  157..172  > > > > > node pattern      (@8:@9)
 @8  158..164  > > > > > > identifier      `person`
 @9  164..171  > > > > > > label           :`Person`
@10  173..403  > > MATCH                   OPTIONAL, pattern=@11, where=@27
@11  188..259  > > > pattern               paths=[@12]
@12  188..259  > > > > pattern path        (@13)-[@15]-(@17)-[@20]-(@24)
@13  188..196  > > > > > node pattern      (@14)
@14  189..195  > > > > > > identifier      `person`
@15  196..213  > > > > > rel pattern       <-[:@16]-
@16  199..211  > > > > > > rel type        :`HAS_CREATOR`
@17  213..230  > > > > > node pattern      (@18:@19)
@18  214..221  > > > > > > identifier      `message`
@19  221..229  > > > > > > label           :`Message`
@20  230..248  > > > > > rel pattern       -[:@21*@22]->
@21  232..241  > > > > > > rel type        :`REPLY_OF`
@22  241..245  > > > > > > range           *@23..
@23  242..243  > > > > > > > integer       0
@24  248..259  > > > > > node pattern      (@25:@26)
@25  249..253  > > > > > > identifier      `post`
@26  253..258  > > > > > > label           :`Post`
@27  266..403  > > > binary operator       @28 AND @44
@28  266..371  > > > > binary operator     @29 AND @39
@29  266..336  > > > > > binary operator   @30 AND @34
@30  266..296  > > > > > > unary operator  IS NOT NULL @31
@31  266..282  > > > > > > > property      @32.@33
@32  266..273  > > > > > > > > identifier  `message`
@33  274..281  > > > > > > > > prop name   `content`
@34  300..336  > > > > > > comparison      @35 < @38
@35  300..315  > > > > > > > property      @36.@37
@36  300..307  > > > > > > > > identifier  `message`
@37  308..314  > > > > > > > > prop name   `length`
@38  317..333  > > > > > > > parameter     $`lengthThreshold`
@39  340..371  > > > > > comparison        @40 > @43
@40  340..361  > > > > > > property        @41.@42
@41  340..347  > > > > > > > identifier    `message`
@42  348..360  > > > > > > > prop name     `creationDate`
@43  363..368  > > > > > > parameter       $`date`
@44  375..403  > > > > binary operator     @45 IN @48
@45  375..389  > > > > > property          @46.@47
@46  375..379  > > > > > > identifier      `post`
@47  380..388  > > > > > > prop name       `language`
@48  392..402  > > > > > parameter         $`languages`
@49  403..451  > > WITH                    projections=[@50, @52]
@50  410..416  > > > projection            expression=@51
@51  410..416  > > > > identifier          `person`
@52  420..451  > > > projection            expression=@53, alias=@56
@53  420..434  > > > > apply               @54(@55)
@54  420..425  > > > > > function name     `count`
@55  426..433  > > > > > identifier        `message`
@56  438..450  > > > > identifier          `messageCount`
@57  451..554  > > RETURN                  projections=[@58, @60], ORDER BY=@65
@58  460..472  > > > projection            expression=@59
@59  460..472  > > > > identifier          `messageCount`
@60  476..505  > > > projection            expression=@61, alias=@64
@61  476..489  > > > > apply               @62(@63)
@62  476..481  > > > > > function name     `count`
@63  482..488  > > > > > identifier        `person`
@64  493..504  > > > > identifier          `personCount`
@65  505..554  > > > ORDER BY              items=[@66, @68]
@66  516..532  > > > > sort item           expression=@67, DESCENDING
@67  516..527  > > > > > identifier        `personCount`
@68  536..554  > > > > sort item           expression=@69, DESCENDING
@69  536..548  > > > > > identifier        `messageCount`
