  @0    0..781  statement                   body=@1
  @1    0..781  > query                     clauses=[@2, @22, @43, @59]
  @2    0..100  > > MATCH                   pattern=@3
  @3    6..99   > > > pattern               paths=[@4]
  @4    6..99   > > > > pattern path        (@5)-[@11]-(@13)-[@16]-(@19)
  @5    6..36   > > > > > node pattern      (@6:@7 {@8})
  @6    7..13   > > > > > > identifier      `person`
  @7   13..20   > > > > > > label           :`Person`
  @8   21..35   > > > > > > map             {@9:@10}
  @9   22..24   > > > > > > > prop name     `id`
 @10   25..34   > > > > > > > parameter     $`personId`
 @11   36..53   > > > > > rel pattern       <-[:@12]-
 @12   39..51   > > > > > > rel type        :`HAS_CREATOR`
 @13   53..70   > > > > > node pattern      (@14:@15)
 @14   54..61   > > > > > > identifier      `message`
 @15   61..69   > > > > > > label           :`Message`
 @16   70..85   > > > > > rel pattern       <-[@17:@18]-
 @17   73..77   > > > > > > identifier      `like`
 @18   77..83   > > > > > > rel type        :`LIKES`
 @19   85..99   > > > > > node pattern      (@20:@21)
 @20   86..91   > > > > > > identifier      `liker`
 @21   91..98   > > > > > > label           :`Person`
 @22  100..209  > > WITH                    projections=[@23, @25, @27, @32], ORDER BY=@34
 @23  105..110  > > > projection            expression=@24
 @24  105..110  > > > > identifier          `liker`
 @25  112..119  > > > projection            expression=@26
 @26  112..119  > > > > identifier          `message`
 @27  121..150  > > > projection            expression=@28, alias=@31
 @28  121..139  > > > > property            @29.@30
 @29  121..125  > > > > > identifier        `like`
 @30  126..138  > > > > > prop name         `creationDate`
 @31  142..150  > > > > identifier          `likeTime`
 @32  152..159  > > > projection            expression=@33
 @33  152..158  > > > > identifier          `person`
 @34  159..209  > > > ORDER BY              items=[@35, @37]
 @35  168..181  > > > > sort item           expression=@36, DESCENDING
 @36  168..176  > > > > > identifier        `likeTime`
 @37  183..209  > > > > sort item           expression=@38, ASCENDING
 @38  183..204  > > > > > apply             @39(@40)
 @39  183..192  > > > > > > function name   `toInteger`
 @40  193..203  > > > > > > property        @41.@42
 @41  193..200  > > > > > > > identifier    `message`
 @42  201..203  > > > > > > > prop name     `id`
 @43  209..299  > > WITH                    projections=[@44, @46, @57]
 @44  216..221  > > > projection            expression=@45
 @45  216..221  > > > > identifier          `liker`
 @46  225..288  > > > projection            expression=@47, alias=@56
 @47  225..274  > > > > apply               @48(@49)
 @48  225..229  > > > > > function name     `head`
 @49  230..273  > > > > > apply             @50(@51)
 @50  230..237  > > > > > > function name   `collect`
 @51  238..272  > > > > > > map             {@52:@53, @54:@55}
 @52  239..242  > > > > > > > prop name     `msg`
 @53  244..251  > > > > > > > identifier    `message`
 @54  253..261  > > > > > > > prop name     `likeTime`
 @55  263..271  > > > > > > > identifier    `likeTime`
 @56  278..288  > > > > identifier          `latestLike`
 @57  292..299  > > > projection            expression=@58
 @58  292..298  > > > > identifier          `person`
 @59  299..781  > > RETURN                  projections=[@60, @65, @70, @75, @80, @87, @108, @115], ORDER BY=@125, LIMIT=@132
 @60  308..328  > > > projection            expression=@61, alias=@64
 @61  308..317  > > > > property            @62.@63
 @62  308..313  > > > > > identifier        `liker`
 @63  314..316  > > > > > prop name         `id`
 @64  320..328  > > > > identifier          `personId`
 @65  332..366  > > > projection            expression=@66, alias=@69
 @66  332..348  > > > > property            @67.@68
 @67  332..337  > > > > > identifier        `liker`
 @68  338..347  > > > > > prop name         `firstName`
 @69  351..366  > > > > identifier          `personFirstName`
 @70  370..402  > > > projection            expression=@71, alias=@74
 @71  370..385  > > > > property            @72.@73
 @72  370..375  > > > > > identifier        `liker`
 @73  376..384  > > > > > prop name         `lastName`
 @74  388..402  > > > > identifier          `personLastName`
 @75  406..445  > > > projection            expression=@76, alias=@79
 @76  406..426  > > > > property            @77.@78
 @77  406..416  > > > > > identifier        `latestLike`
 @78  417..425  > > > > > prop name         `likeTime`
 @79  429..445  > > > > identifier          `likeCreationDate`
 @80  449..479  > > > projection            expression=@81, alias=@86
 @81  449..467  > > > > property            @82.@85
 @82  449..463  > > > > > property          @83.@84
 @83  449..459  > > > > > > identifier      `latestLike`
 @84  460..463  > > > > > > prop name       `msg`
 @85  464..466  > > > > > prop name         `id`
 @86  470..479  > > > > identifier          `messageId`
 @87  483..618  > > > projection            expression=@88, alias=@107
 @88  483..600  > > > > case                expression=@89, alternatives=[(@96:@97)], default=@102
 @89  488..518  > > > > > apply             @90(@91)
 @90  488..494  > > > > > > function name   `exists`
 @91  495..517  > > > > > > property        @92.@95
 @92  495..509  > > > > > > > property      @93.@94
 @93  495..505  > > > > > > > > identifier  `latestLike`
 @94  506..509  > > > > > > > > prop name   `msg`
 @95  510..517  > > > > > > > prop name     `content`
 @96  528..532  > > > > > TRUE
 @97  538..565  > > > > > property          @98.@101
 @98  538..552  > > > > > > property        @99.@100
 @99  538..548  > > > > > > > identifier    `latestLike`
@100  549..552  > > > > > > > prop name     `msg`
@101  553..560  > > > > > > prop name       `content`
@102  570..597  > > > > > property          @103.@106
@103  570..584  > > > > > > property        @104.@105
@104  570..580  > > > > > > > identifier    `latestLike`
@105  581..584  > > > > > > > prop name     `msg`
@106  585..594  > > > > > > prop name       `imageFile`
@107  604..618  > > > > identifier          `messageContent`
@108  622..672  > > > projection            expression=@109, alias=@114
@109  622..650  > > > > property            @110.@113
@110  622..636  > > > > > property          @111.@112
@111  622..632  > > > > > > identifier      `latestLike`
@112  633..636  > > > > > > prop name       `msg`
@113  637..649  > > > > > prop name         `creationDate`
@114  653..672  > > > > identifier          `messageCreationDate`
@115  676..716  > > > projection            expression=@116, alias=@124
@116  676..707  > > > > unary operator      NOT @117
@117  680..705  > > > > > pattern path      (@118)-[@120]-(@122)
@118  680..687  > > > > > > node pattern    (@119)
@119  681..686  > > > > > > > identifier    `liker`
@120  687..697  > > > > > > rel pattern     -[:@121]-
@121  689..695  > > > > > > > rel type      :`KNOWS`
@122  697..705  > > > > > > node pattern    (@123)
@123  698..704  > > > > > > > identifier    `person`
@124  710..715  > > > > identifier          `isNew`
@125  716..772  > > > ORDER BY              items=[@126, @128]
@126  725..746  > > > > sort item           expression=@127, DESCENDING
@127  725..741  > > > > > identifier        `likeCreationDate`
@128  748..772  > > > > sort item           expression=@129, ASCENDING
@129  748..767  > > > > > apply             @130(@131)
@130  748..757  > > > > > > function name   `toInteger`
@131  758..766  > > > > > > identifier      `personId`
@132  778..780  > > > integer               20
