  @0    2..31   line_comment             // Q5. Top posters in a country
  @1   34..67   block_comment            /*\n  :param { country: 'Belarus' }\n*/
  @2   70..707  statement                body=@3
  @3   70..707  > query                  clauses=[@4, @26, @42, @48, @51, @61, @79]
  @4   70..195  > > MATCH                pattern=@5
  @5   78..194  > > > pattern            paths=[@6]
  @6   78..194  > > > > pattern path     (@7)-[@12]-(@14)-[@16]-(@18)-[@21]-(@23)
  @7   78..105  > > > > > node pattern   (:@8 {@9})
  @8   79..87   > > > > > > label        :`Country`
  @9   88..104  > > > > > > map          {@10:@11}
 @10   89..93   > > > > > > > prop name  `name`
 @11   95..103  > > > > > > > parameter  $`country`
 @12  105..121  > > > > > rel pattern    <-[:@13]-
 @13  108..119  > > > > > > rel type     :`IS_PART_OF`
 @14  121..128  > > > > > node pattern   (:@15)
 @15  122..127  > > > > > > label        :`City`
 @16  128..147  > > > > > rel pattern    <-[:@17]-
 @17  131..145  > > > > > > rel type     :`IS_LOCATED_IN`
 @18  150..165  > > > > > node pattern   (@19:@20)
 @19  151..157  > > > > > > identifier   `person`
 @20  157..164  > > > > > > label        :`Person`
 @21  165..181  > > > > > rel pattern    <-[:@22]-
 @22  168..179  > > > > > > rel type     :`HAS_MEMBER`
 @23  181..194  > > > > > node pattern   (@24:@25)
 @24  182..187  > > > > > > identifier   `forum`
 @25  187..193  > > > > > > label        :`Forum`
 @26  195..294  > > WITH                 projections=[@27, @29], ORDER BY=@34, LIMIT=@41
 @27  200..205  > > > projection         expression=@28
 @28  200..205  > > > > identifier       `forum`
 @29  207..240  > > > projection         expression=@30, alias=@33
 @30  207..220  > > > > apply            @31(@32)
 @31  207..212  > > > > > function name  `count`
 @32  213..219  > > > > > identifier     `person`
 @33  224..239  > > > > identifier       `numberOfMembers`
 @34  240..284  > > > ORDER BY           items=[@35, @37]
 @35  249..269  > > > > sort item        expression=@36, DESCENDING
 @36  249..264  > > > > > identifier     `numberOfMembers`
 @37  271..284  > > > > sort item        expression=@38, ASCENDING
 @38  271..280  > > > > > property       @39.@40
 @39  271..276  > > > > > > identifier   `forum`
 @40  277..279  > > > > > > prop name    `id`
 @41  290..293  > > > integer            100
 @42  294..331  > > WITH                 projections=[@43]
 @43  299..331  > > > projection         expression=@44, alias=@47
 @44  299..313  > > > > apply            @45(@46)
 @45  299..306  > > > > > function name  `collect`
 @46  307..312  > > > > > identifier     `forum`
 @47  317..330  > > > > identifier       `popularForums`
 @48  331..361  > > UNWIND               expression=@49, alias=@50
 @49  338..351  > > > identifier         `popularForums`
 @50  355..360  > > > identifier         `forum`
 @51  361..408  > > MATCH                pattern=@52
 @52  369..407  > > > pattern            paths=[@53]
 @53  369..407  > > > > pattern path     (@54)-[@56]-(@58)
 @54  369..376  > > > > > node pattern   (@55)
 @55  370..375  > > > > > > identifier   `forum`
 @56  376..392  > > > > > rel pattern    -[:@57]->
 @57  378..389  > > > > > > rel type     :`HAS_MEMBER`
 @58  392..407  > > > > > node pattern   (@59:@60)
 @59  393..399  > > > > > > identifier   `person`
 @60  399..406  > > > > > > label        :`Person`
 @61  408..536  > > MATCH                OPTIONAL, pattern=@62, where=@76
 @62  425..499  > > > pattern            paths=[@63]
 @63  425..499  > > > > pattern path     (@64)-[@66]-(@68)-[@71]-(@73)
 @64  425..433  > > > > > node pattern   (@65)
 @65  426..432  > > > > > > identifier   `person`
 @66  433..450  > > > > > rel pattern    <-[:@67]-
 @67  436..448  > > > > > > rel type     :`HAS_CREATOR`
 @68  450..461  > > > > > node pattern   (@69:@70)
 @69  451..455  > > > > > > identifier   `post`
 @70  455..460  > > > > > > label        :`Post`
 @71  461..479  > > > > > rel pattern    <-[:@72]-
 @72  464..477  > > > > > > rel type     :`CONTAINER_OF`
 @73  479..499  > > > > > node pattern   (@74:@75)
 @74  480..492  > > > > > > identifier   `popularForum`
 @75  492..498  > > > > > > label        :`Forum`
 @76  506..536  > > > binary operator    @77 IN @78
 @77  506..518  > > > > identifier       `popularForum`
 @78  522..535  > > > > identifier       `popularForums`
 @79  536..707  > > RETURN               projections=[@80, @85, @90, @95, @100], ORDER BY=@105, LIMIT=@112
 @80  545..554  > > > projection         expression=@81, alias=@84
 @81  545..554  > > > > property         @82.@83
 @82  545..551  > > > > > identifier     `person`
 @83  552..554  > > > > > prop name      `id`
 @84  545..554  > > > > identifier       `person.id`
 @85  558..574  > > > projection         expression=@86, alias=@89
 @86  558..574  > > > > property         @87.@88
 @87  558..564  > > > > > identifier     `person`
 @88  565..574  > > > > > prop name      `firstName`
 @89  558..574  > > > > identifier       `person.firstName`
 @90  578..593  > > > projection         expression=@91, alias=@94
 @91  578..593  > > > > property         @92.@93
 @92  578..584  > > > > > identifier     `person`
 @93  585..593  > > > > > prop name      `lastName`
 @94  578..593  > > > > identifier       `person.lastName`
 @95  597..616  > > > projection         expression=@96, alias=@99
 @96  597..616  > > > > property         @97.@98
 @97  597..603  > > > > > identifier     `person`
 @98  604..616  > > > > > prop name      `creationDate`
 @99  597..616  > > > > identifier       `person.creationDate`
@100  620..654  > > > projection         expression=@101, alias=@104
@101  620..640  > > > > apply            @102(DISTINCT @103)
@102  620..625  > > > > > function name  `count`
@103  635..639  > > > > > identifier     `post`
@104  644..653  > > > > identifier       `postCount`
@105  654..697  > > > ORDER BY           items=[@106, @108]
@106  665..679  > > > > sort item        expression=@107, DESCENDING
@107  665..674  > > > > > identifier     `postCount`
@108  683..697  > > > > sort item        expression=@109, ASCENDING
@109  683..693  > > > > > property       @110.@111
@110  683..689  > > > > > > identifier   `person`
@111  690..692  > > > > > > prop name    `id`
@112  703..706  > > > integer            100
