  @0    0..689  statement                   body=@1
  @1    0..689  > query                     clauses=[@2, @11, @42, @49, @52, @61, @70, @77, @80, @91, @105, @112, @115, @126]
  @2    0..28   > > MATCH                   pattern=@3
  @3    6..27   > > > pattern               paths=[@4]
  @4    6..27   > > > > pattern path        (@5)
  @5    6..27   > > > > > node pattern      (@6:@7 {@8})
  @6    7..8    > > > > > > identifier      `c`
  @7    8..13   > > > > > > label           :`City`
  @8   14..26   > > > > > > map             {@9:@10}
  @9   15..17   > > > > > > > prop name     `id`
 @10   18..25   > > > > > > > parameter     $`cityId`
 @11   28..296  > > CREATE                  pattern=@12
 @12   35..295  > > > pattern               paths=[@13]
 @13   35..295  > > > > pattern path        (@14)-[@38]-(@40)
 @14   35..273  > > > > > node pattern      (@15:@16 {@17})
 @15   36..37   > > > > > > identifier      `p`
 @16   37..44   > > > > > > label           :`Person`
 @17   45..272  > > > > > > map             {@18:@19, @20:@21, @22:@23, @24:@25, @26:@27, @28:@29, @30:@31, @32:@33, @34:@35, @36:@37}
 @18   46..48   > > > > > > > prop name     `id`
 @19   50..59   > > > > > > > parameter     $`personId`
 @20   61..70   > > > > > > > prop name     `firstName`
 @21   72..88   > > > > > > > parameter     $`personFirstName`
 @22   90..98   > > > > > > > prop name     `lastName`
 @23  100..115  > > > > > > > parameter     $`personLastName`
 @24  117..123  > > > > > > > prop name     `gender`
 @25  125..132  > > > > > > > parameter     $`gender`
 @26  134..142  > > > > > > > prop name     `birthday`
 @27  144..153  > > > > > > > parameter     $`birthday`
 @28  155..167  > > > > > > > prop name     `creationDate`
 @29  169..182  > > > > > > > parameter     $`creationDate`
 @30  184..194  > > > > > > > prop name     `locationIP`
 @31  196..207  > > > > > > > parameter     $`locationIP`
 @32  209..220  > > > > > > > prop name     `browserUsed`
 @33  222..234  > > > > > > > parameter     $`browserUsed`
 @34  236..242  > > > > > > > prop name     `speaks`
 @35  244..254  > > > > > > > parameter     $`languages`
 @36  256..262  > > > > > > > prop name     `emails`
 @37  264..271  > > > > > > > parameter     $`emails`
 @38  273..292  > > > > > rel pattern       -[:@39]->
 @39  275..289  > > > > > > rel type        :`IS_LOCATED_IN`
 @40  292..295  > > > > > node pattern      (@41)
 @41  293..294  > > > > > > identifier      `c`
 @42  296..323  > > WITH                    projections=[@43, @45]
 @43  301..302  > > > projection            expression=@44
 @44  301..302  > > > > identifier          `p`
 @45  304..323  > > > projection            expression=@46, alias=@48
 @46  304..312  > > > > apply all           @47(*)
 @47  304..309  > > > > > function name     `count`
 @48  316..322  > > > > identifier          `dummy1`
 @49  323..351  > > UNWIND                  expression=@50, alias=@51
 @50  330..337  > > > parameter             $`tagIds`
 @51  341..346  > > > identifier            `tagId`
 @52  351..381  > > MATCH                   pattern=@53
 @53  357..376  > > > pattern               paths=[@54]
 @54  357..376  > > > > pattern path        (@55)
 @55  357..376  > > > > > node pattern      (@56:@57 {@58})
 @56  358..359  > > > > > > identifier      `t`
 @57  359..363  > > > > > > label           :`Tag`
 @58  364..375  > > > > > > map             {@59:@60}
 @59  365..367  > > > > > > > prop name     `id`
 @60  369..374  > > > > > > > identifier    `tagId`
 @61  381..413  > > CREATE                  pattern=@62
 @62  388..412  > > > pattern               paths=[@63]
 @63  388..412  > > > > pattern path        (@64)-[@66]-(@68)
 @64  388..391  > > > > > node pattern      (@65)
 @65  389..390  > > > > > > identifier      `p`
 @66  391..409  > > > > > rel pattern       -[:@67]->
 @67  393..406  > > > > > > rel type        :`HAS_INTEREST`
 @68  409..412  > > > > > node pattern      (@69)
 @69  410..411  > > > > > > identifier      `t`
 @70  413..440  > > WITH                    projections=[@71, @73]
 @71  418..419  > > > projection            expression=@72
 @72  418..419  > > > > identifier          `p`
 @73  421..440  > > > projection            expression=@74, alias=@76
 @74  421..429  > > > > apply all           @75(*)
 @75  421..426  > > > > > function name     `count`
 @76  433..439  > > > > identifier          `dummy2`
 @77  440..465  > > UNWIND                  expression=@78, alias=@79
 @78  447..455  > > > parameter             $`studyAt`
 @79  459..460  > > > identifier            `s`
 @80  465..503  > > MATCH                   pattern=@81
 @81  471..498  > > > pattern               paths=[@82]
 @82  471..498  > > > > pattern path        (@83)
 @83  471..498  > > > > > node pattern      (@84:@85 {@86})
 @84  472..473  > > > > > > identifier      `u`
 @85  473..486  > > > > > > label           :`Organisation`
 @86  487..497  > > > > > > map             {@87:@88}
 @87  488..490  > > > > > > > prop name     `id`
 @88  492..496  > > > > > > > subscript     @89[@90]
 @89  492..493  > > > > > > > > identifier  `s`
 @90  494..495  > > > > > > > > integer     0
 @91  503..549  > > CREATE                  pattern=@92
 @92  510..548  > > > pattern               paths=[@93]
 @93  510..548  > > > > pattern path        (@94)-[@96]-(@103)
 @94  510..513  > > > > > node pattern      (@95)
 @95  511..512  > > > > > > identifier      `p`
 @96  513..545  > > > > > rel pattern       -[:@97 {@98}]->
 @97  515..524  > > > > > > rel type        :`STUDY_AT`
 @98  525..542  > > > > > > map             {@99:@100}
 @99  526..535  > > > > > > > prop name     `classYear`
@100  537..541  > > > > > > > subscript     @101[@102]
@101  537..538  > > > > > > > > identifier  `s`
@102  539..540  > > > > > > > > integer     1
@103  545..548  > > > > > node pattern      (@104)
@104  546..547  > > > > > > identifier      `u`
@105  549..576  > > WITH                    projections=[@106, @108]
@106  554..555  > > > projection            expression=@107
@107  554..555  > > > > identifier          `p`
@108  557..576  > > > projection            expression=@109, alias=@111
@109  557..565  > > > > apply all           @110(*)
@110  557..562  > > > > > function name     `count`
@111  569..575  > > > > identifier          `dummy3`
@112  576..600  > > UNWIND                  expression=@113, alias=@114
@113  583..590  > > > parameter             $`workAt`
@114  594..595  > > > identifier            `w`
@115  600..641  > > MATCH                   pattern=@116
@116  606..636  > > > pattern               paths=[@117]
@117  606..636  > > > > pattern path        (@118)
@118  606..636  > > > > > node pattern      (@119:@120 {@121})
@119  607..611  > > > > > > identifier      `comp`
@120  611..624  > > > > > > label           :`Organisation`
@121  625..635  > > > > > > map             {@122:@123}
@122  626..628  > > > > > > > prop name     `id`
@123  630..634  > > > > > > > subscript     @124[@125]
@124  630..631  > > > > > > > > identifier  `w`
@125  632..633  > > > > > > > > integer     0
@126  641..689  > > CREATE                  pattern=@127
@127  648..688  > > > pattern               paths=[@128]
@128  648..688  > > > > pattern path        (@129)-[@131]-(@138)
@129  648..651  > > > > > node pattern      (@130)
@130  649..650  > > > > > > identifier      `p`
@131  651..682  > > > > > rel pattern       -[:@132 {@133}]->
@132  653..662  > > > > > > rel type        :`WORKS_AT`
@133  663..679  > > > > > > map             {@134:@135}
@134  664..672  > > > > > > > prop name     `workFrom`
@135  674..678  > > > > > > > subscript     @136[@137]
@136  674..675  > > > > > > > > identifier  `w`
@137  676..677  > > > > > > > > integer     1
@138  682..688  > > > > > node pattern      (@139)
@139  683..687  > > > > > > identifier      `comp`
