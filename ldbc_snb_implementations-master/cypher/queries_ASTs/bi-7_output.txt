 @0    2..48   line_comment             // Q7. Most authoritative users on a given topic
 @1   51..94   block_comment            /*\n  :param { tag: 'Arnold_Schwarzenegger' }\n*/
 @2   97..531  statement                body=@3
 @3   97..531  > query                  clauses=[@4, @13, @28, @42, @52, @68]
 @4   97..126  > > MATCH                pattern=@5
 @5  103..125  > > > pattern            paths=[@6]
 @6  103..125  > > > > pattern path     (@7)
 @7  103..125  > > > > > node pattern   (@8:@9 {@10})
 @8  104..107  > > > > > > identifier   `tag`
 @9  107..111  > > > > > > label        :`Tag`
@10  112..124  > > > > > > map          {@11:@12}
@11  113..117  > > > > > > > prop name  `name`
@12  119..123  > > > > > > > parameter  $`tag`
@13  126..202  > > MATCH                pattern=@14
@14  132..201  > > > pattern            paths=[@15]
@15  132..201  > > > > pattern path     (@16)-[@18]-(@20)-[@23]-(@25)
@16  132..137  > > > > > node pattern   (@17)
@17  133..136  > > > > > > identifier   `tag`
@18  137..150  > > > > > rel pattern    <-[:@19]-
@19  140..148  > > > > > > rel type     :`HAS_TAG`
@20  150..168  > > > > > node pattern   (@21:@22)
@21  151..159  > > > > > > identifier   `message1`
@22  159..167  > > > > > > label        :`Message`
@23  168..185  > > > > > rel pattern    -[:@24]->
@24  170..182  > > > > > > rel type     :`HAS_CREATOR`
@25  185..201  > > > > > node pattern   (@26:@27)
@26  186..193  > > > > > > identifier   `person1`
@27  193..200  > > > > > > label        :`Person`
@28  202..271  > > MATCH                pattern=@29
@29  208..270  > > > pattern            paths=[@30]
@30  208..270  > > > > pattern path     (@31)-[@33]-(@35)-[@38]-(@40)
@31  208..213  > > > > > node pattern   (@32)
@32  209..212  > > > > > > identifier   `tag`
@33  213..226  > > > > > rel pattern    <-[:@34]-
@34  216..224  > > > > > > rel type     :`HAS_TAG`
@35  226..244  > > > > > node pattern   (@36:@37)
@36  227..235  > > > > > > identifier   `message2`
@37  235..243  > > > > > > label        :`Message`
@38  244..261  > > > > > rel pattern    -[:@39]->
@39  246..258  > > > > > > rel type     :`HAS_CREATOR`
@40  261..270  > > > > > node pattern   (@41)
@41  262..269  > > > > > > identifier   `person1`
@42  271..324  > > MATCH                OPTIONAL, pattern=@43
@43  286..323  > > > pattern            paths=[@44]
@44  286..323  > > > > pattern path     (@45)-[@47]-(@49)
@45  286..296  > > > > > node pattern   (@46)
@46  287..295  > > > > > > identifier   `message2`
@47  296..307  > > > > > rel pattern    <-[:@48]-
@48  299..305  > > > > > > rel type     :`LIKES`
@49  307..323  > > > > > node pattern   (@50:@51)
@50  308..315  > > > > > > identifier   `person2`
@51  315..322  > > > > > > label        :`Person`
@52  324..410  > > MATCH                OPTIONAL, pattern=@53
@53  339..409  > > > pattern            paths=[@54]
@54  339..409  > > > > pattern path     (@55)-[@57]-(@59)-[@62]-(@65)
@55  339..348  > > > > > node pattern   (@56)
@56  340..347  > > > > > > identifier   `person2`
@57  348..365  > > > > > rel pattern    <-[:@58]-
@58  351..363  > > > > > > rel type     :`HAS_CREATOR`
@59  365..383  > > > > > node pattern   (@60:@61)
@60  366..374  > > > > > > identifier   `message3`
@61  374..382  > > > > > > label        :`Message`
@62  383..398  > > > > > rel pattern    <-[@63:@64]-
@63  386..390  > > > > > > identifier   `like`
@64  390..396  > > > > > > rel type     :`LIKES`
@65  398..409  > > > > > node pattern   (@66:@67)
@66  399..401  > > > > > > identifier   `p3`
@67  401..408  > > > > > > label        :`Person`
@68  410..531  > > RETURN               projections=[@69, @74], ORDER BY=@79, LIMIT=@86
@69  419..429  > > > projection         expression=@70, alias=@73
@70  419..429  > > > > property         @71.@72
@71  419..426  > > > > > identifier     `person1`
@72  427..429  > > > > > prop name      `id`
@73  419..429  > > > > identifier       `person1.id`
@74  433..472  > > > projection         expression=@75, alias=@78
@75  433..453  > > > > apply            @76(DISTINCT @77)
@76  433..438  > > > > > function name  `count`
@77  448..452  > > > > > identifier     `like`
@78  457..471  > > > > identifier       `authorityScore`
@79  472..521  > > > ORDER BY           items=[@80, @82]
@80  483..502  > > > > sort item        expression=@81, DESCENDING
@81  483..497  > > > > > identifier     `authorityScore`
@82  506..521  > > > > sort item        expression=@83, ASCENDING
@83  506..517  > > > > > property       @84.@85
@84  506..513  > > > > > > identifier   `person1`
@85  514..516  > > > > > > prop name    `id`
@86  527..530  > > > integer            100
