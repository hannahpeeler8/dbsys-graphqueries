 @0    2..25   line_comment              // Q20. High-level topics
 @1   28..86   block_comment             /*\n  :param { tagClasses: ['Writer', 'Single', 'Country'] }\n*/
 @2   89..390  statement                 body=@3
 @3   89..390  > query                   clauses=[@4, @7, @32]
 @4   89..124  > > UNWIND                expression=@5, alias=@6
 @5   96..107  > > > parameter           $`tagClasses`
 @6  111..123  > > > identifier          `tagClassName`
 @7  124..264  > > MATCH                 pattern=@8
 @8  132..263  > > > pattern             paths=[@9]
 @9  132..263  > > > > pattern path      (@10)-[@16]-(@20)-[@22]-(@24)-[@27]-(@29)
@10  132..172  > > > > > node pattern    (@11:@12 {@13})
@11  133..141  > > > > > > identifier    `tagClass`
@12  141..150  > > > > > > label         :`TagClass`
@13  151..171  > > > > > > map           {@14:@15}
@14  152..156  > > > > > > > prop name   `name`
@15  158..170  > > > > > > > identifier  `tagClassName`
@16  172..196  > > > > > rel pattern     <-[:@17*@18]-
@17  175..190  > > > > > > rel type      :`IS_SUBCLASS_OF`
@18  190..194  > > > > > > range         *@19..
@19  191..192  > > > > > > > integer     0
@20  199..210  > > > > > node pattern    (:@21)
@21  200..209  > > > > > > label         :`TagClass`
@22  210..224  > > > > > rel pattern     <-[:@23]-
@23  213..222  > > > > > > rel type      :`HAS_TYPE`
@24  224..233  > > > > > node pattern    (@25:@26)
@25  225..228  > > > > > > identifier    `tag`
@26  228..232  > > > > > > label         :`Tag`
@27  233..246  > > > > > rel pattern     <-[:@28]-
@28  236..244  > > > > > > rel type      :`HAS_TAG`
@29  246..263  > > > > > node pattern    (@30:@31)
@30  247..254  > > > > > > identifier    `message`
@31  254..262  > > > > > > label         :`Message`
@32  264..390  > > RETURN                projections=[@33, @38], ORDER BY=@43, LIMIT=@50
@33  273..286  > > > projection          expression=@34, alias=@37
@34  273..286  > > > > property          @35.@36
@35  273..281  > > > > > identifier      `tagClass`
@36  282..286  > > > > > prop name       `name`
@37  273..286  > > > > identifier        `tagClass.name`
@38  290..330  > > > projection          expression=@39, alias=@42
@39  290..313  > > > > apply             @40(DISTINCT @41)
@40  290..295  > > > > > function name   `count`
@41  305..312  > > > > > identifier      `message`
@42  317..329  > > > > identifier        `messageCount`
@43  330..380  > > > ORDER BY            items=[@44, @46]
@44  341..358  > > > > sort item         expression=@45, DESCENDING
@45  341..353  > > > > > identifier      `messageCount`
@46  362..380  > > > > sort item         expression=@47, ASCENDING
@47  362..376  > > > > > property        @48.@49
@48  362..370  > > > > > > identifier    `tagClass`
@49  371..375  > > > > > > prop name     `name`
@50  386..389  > > > integer             100
