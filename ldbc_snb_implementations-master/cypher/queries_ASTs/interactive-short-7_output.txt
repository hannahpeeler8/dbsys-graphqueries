 @0    0..491  statement                body=@1
 @1    0..491  > query                  clauses=[@2, @21, @36]
 @2    0..86   > > MATCH                pattern=@3
 @3    6..85   > > > pattern            paths=[@4]
 @4    6..85   > > > > pattern path     (@5)-[@11]-(@13)-[@16]-(@18)
 @5    6..33   > > > > > node pattern   (@6:@7 {@8})
 @6    7..8    > > > > > > identifier   `m`
 @7    8..16   > > > > > > label        :`Message`
 @8   17..32   > > > > > > map          {@9:@10}
 @9   18..20   > > > > > > > prop name  `id`
@10   21..31   > > > > > > > parameter  $`messageId`
@11   33..47   > > > > > rel pattern    <-[:@12]-
@12   36..45   > > > > > > rel type     :`REPLY_OF`
@13   47..58   > > > > > node pattern   (@14:@15)
@14   48..49   > > > > > > identifier   `c`
@15   49..57   > > > > > > label        :`Comment`
@16   58..75   > > > > > rel pattern    -[:@17]->
@17   60..72   > > > > > > rel type     :`HAS_CREATOR`
@18   75..85   > > > > > node pattern   (@19:@20)
@19   76..77   > > > > > > identifier   `p`
@20   77..84   > > > > > > label        :`Person`
@21   86..146  > > MATCH                OPTIONAL, pattern=@22
@22  101..145  > > > pattern            paths=[@23]
@23  101..145  > > > > pattern path     (@24)-[@26]-(@28)-[@31]-(@34)
@24  101..104  > > > > > node pattern   (@25)
@25  102..103  > > > > > > identifier   `m`
@26  104..121  > > > > > rel pattern    -[:@27]->
@27  106..118  > > > > > > rel type     :`HAS_CREATOR`
@28  121..131  > > > > > node pattern   (@29:@30)
@29  122..123  > > > > > > identifier   `a`
@30  123..130  > > > > > > label        :`Person`
@31  131..142  > > > > > rel pattern    -[@32:@33]-
@32  133..134  > > > > > > identifier   `r`
@33  134..140  > > > > > > rel type     :`KNOWS`
@34  142..145  > > > > > node pattern   (@35)
@35  143..144  > > > > > > identifier   `p`
@36  146..491  > > RETURN               projections=[@37, @42, @47, @52, @57, @62, @67], ORDER BY=@74
@37  155..172  > > > projection         expression=@38, alias=@41
@38  155..160  > > > > property         @39.@40
@39  155..156  > > > > > identifier     `c`
@40  157..159  > > > > > prop name      `id`
@41  163..172  > > > > identifier       `commentId`
@42  176..203  > > > projection         expression=@43, alias=@46
@43  176..186  > > > > property         @44.@45
@44  176..177  > > > > > identifier     `c`
@45  178..185  > > > > > prop name      `content`
@46  189..203  > > > > identifier       `commentContent`
@47  207..244  > > > projection         expression=@48, alias=@51
@48  207..222  > > > > property         @49.@50
@49  207..208  > > > > > identifier     `c`
@50  209..221  > > > > > prop name      `creationDate`
@51  225..244  > > > > identifier       `commentCreationDate`
@52  248..269  > > > projection         expression=@53, alias=@56
@53  248..253  > > > > property         @54.@55
@54  248..249  > > > > > identifier     `p`
@55  250..252  > > > > > prop name      `id`
@56  256..269  > > > > identifier       `replyAuthorId`
@57  273..308  > > > projection         expression=@58, alias=@61
@58  273..285  > > > > property         @59.@60
@59  273..274  > > > > > identifier     `p`
@60  275..284  > > > > > prop name      `firstName`
@61  288..308  > > > > identifier       `replyAuthorFirstName`
@62  312..345  > > > projection         expression=@63, alias=@66
@63  312..323  > > > > property         @64.@65
@64  312..313  > > > > > identifier     `p`
@65  314..322  > > > > > prop name      `lastName`
@66  326..345  > > > > identifier       `replyAuthorLastName`
@67  349..442  > > > projection         expression=@68, alias=@73
@68  349..400  > > > > case             expression=@69, alternatives=[(@70:@71)], default=@72
@69  354..355  > > > > > identifier     `r`
@70  365..369  > > > > > NULL
@71  375..380  > > > > > FALSE
@72  390..394  > > > > > TRUE
@73  404..441  > > > > identifier       `replyAuthorKnowsOriginalMessageAuthor`
@74  442..491  > > > ORDER BY           items=[@75, @77]
@75  451..475  > > > > sort item        expression=@76, DESCENDING
@76  451..470  > > > > > identifier     `commentCreationDate`
@77  477..491  > > > > sort item        expression=@78, ASCENDING
@78  477..490  > > > > > identifier     `replyAuthorId`
