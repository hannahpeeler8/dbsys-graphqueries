 @0    2..24   line_comment             // Q17. Friend triangles
 @1   27..58   block_comment            /*\n  :param { country: 'Spain' }\n*/
 @2   61..477  statement                body=@3
 @3   61..477  > query                  clauses=[@4, @13, @27, @41, @55, @93]
 @4   61..102  > > MATCH                pattern=@5
 @5   67..101  > > > pattern            paths=[@6]
 @6   67..101  > > > > pattern path     (@7)
 @7   67..101  > > > > > node pattern   (@8:@9 {@10})
 @8   68..75   > > > > > > identifier   `country`
 @9   75..83   > > > > > > label        :`Country`
@10   84..100  > > > > > > map          {@11:@12}
@11   85..89   > > > > > > > prop name  `name`
@12   91..99   > > > > > > > parameter  $`country`
@13  102..170  > > MATCH                pattern=@14
@14  108..169  > > > pattern            paths=[@15]
@15  108..169  > > > > pattern path     (@16)-[@19]-(@21)-[@23]-(@25)
@16  108..118  > > > > > node pattern   (@17:@18)
@17  109..110  > > > > > > identifier   `a`
@18  110..117  > > > > > > label        :`Person`
@19  118..137  > > > > > rel pattern    -[:@20]->
@20  120..134  > > > > > > rel type     :`IS_LOCATED_IN`
@21  137..144  > > > > > node pattern   (:@22)
@22  138..143  > > > > > > label        :`City`
@23  144..160  > > > > > rel pattern    -[:@24]->
@24  146..157  > > > > > > rel type     :`IS_PART_OF`
@25  160..169  > > > > > node pattern   (@26)
@26  161..168  > > > > > > identifier   `country`
@27  170..238  > > MATCH                pattern=@28
@28  176..237  > > > pattern            paths=[@29]
@29  176..237  > > > > pattern path     (@30)-[@33]-(@35)-[@37]-(@39)
@30  176..186  > > > > > node pattern   (@31:@32)
@31  177..178  > > > > > > identifier   `b`
@32  178..185  > > > > > > label        :`Person`
@33  186..205  > > > > > rel pattern    -[:@34]->
@34  188..202  > > > > > > rel type     :`IS_LOCATED_IN`
@35  205..212  > > > > > node pattern   (:@36)
@36  206..211  > > > > > > label        :`City`
@37  212..228  > > > > > rel pattern    -[:@38]->
@38  214..225  > > > > > > rel type     :`IS_PART_OF`
@39  228..237  > > > > > node pattern   (@40)
@40  229..236  > > > > > > identifier   `country`
@41  238..306  > > MATCH                pattern=@42
@42  244..305  > > > pattern            paths=[@43]
@43  244..305  > > > > pattern path     (@44)-[@47]-(@49)-[@51]-(@53)
@44  244..254  > > > > > node pattern   (@45:@46)
@45  245..246  > > > > > > identifier   `c`
@46  246..253  > > > > > > label        :`Person`
@47  254..273  > > > > > rel pattern    -[:@48]->
@48  256..270  > > > > > > rel type     :`IS_LOCATED_IN`
@49  273..280  > > > > > node pattern   (:@50)
@50  274..279  > > > > > > label        :`City`
@51  280..296  > > > > > rel pattern    -[:@52]->
@52  282..293  > > > > > > rel type     :`IS_PART_OF`
@53  296..305  > > > > > node pattern   (@54)
@54  297..304  > > > > > > identifier   `country`
@55  306..401  > > MATCH                pattern=@56, where=@78
@56  312..364  > > > pattern            paths=[@57, @64, @71]
@57  312..328  > > > > pattern path     (@58)-[@60]-(@62)
@58  312..315  > > > > > node pattern   (@59)
@59  313..314  > > > > > > identifier   `a`
@60  315..325  > > > > > rel pattern    -[:@61]-
@61  317..323  > > > > > > rel type     :`KNOWS`
@62  325..328  > > > > > node pattern   (@63)
@63  326..327  > > > > > > identifier   `b`
@64  330..346  > > > > pattern path     (@65)-[@67]-(@69)
@65  330..333  > > > > > node pattern   (@66)
@66  331..332  > > > > > > identifier   `b`
@67  333..343  > > > > > rel pattern    -[:@68]-
@68  335..341  > > > > > > rel type     :`KNOWS`
@69  343..346  > > > > > node pattern   (@70)
@70  344..345  > > > > > > identifier   `c`
@71  348..364  > > > > pattern path     (@72)-[@74]-(@76)
@72  348..351  > > > > > node pattern   (@73)
@73  349..350  > > > > > > identifier   `c`
@74  351..361  > > > > > rel pattern    -[:@75]-
@75  353..359  > > > > > > rel type     :`KNOWS`
@76  361..364  > > > > > node pattern   (@77)
@77  362..363  > > > > > > identifier   `a`
@78  371..401  > > > binary operator    @79 AND @86
@79  371..385  > > > > comparison       @80 < @83
@80  371..376  > > > > > property       @81.@82
@81  371..372  > > > > > > identifier   `a`
@82  373..375  > > > > > > prop name    `id`
@83  378..385  > > > > > property       @84.@85
@84  378..379  > > > > > > identifier   `b`
@85  380..382  > > > > > > prop name    `id`
@86  389..401  > > > > comparison       @87 < @90
@87  389..394  > > > > > property       @88.@89
@88  389..390  > > > > > > identifier   `b`
@89  391..393  > > > > > > prop name    `id`
@90  396..401  > > > > > property       @91.@92
@91  396..397  > > > > > > identifier   `c`
@92  398..400  > > > > > > prop name    `id`
@93  401..477  > > RETURN               projections=[@94]
@94  408..477  > > > projection         expression=@95, alias=@97
@95  408..416  > > > > apply all        @96(*)
@96  408..413  > > > > > function name  `count`
@97  420..425  > > > > identifier       `count`
@98  428..476  > > > > line_comment     // as a less elegant solution, count(a) also works
