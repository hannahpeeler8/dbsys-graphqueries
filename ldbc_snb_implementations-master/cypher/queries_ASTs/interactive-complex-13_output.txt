 @0    0..230  statement                   body=@1
 @1    0..230  > query                     clauses=[@2, @18, @31]
 @2    0..73   > > MATCH                   pattern=@3
 @3    6..72   > > > pattern               paths=[@4, @11]
 @4    6..38   > > > > pattern path        (@5)
 @5    6..38   > > > > > node pattern      (@6:@7 {@8})
 @6    7..14   > > > > > > identifier      `person1`
 @7   14..21   > > > > > > label           :`Person`
 @8   22..37   > > > > > > map             {@9:@10}
 @9   23..25   > > > > > > > prop name     `id`
@10   26..36   > > > > > > > parameter     $`person1Id`
@11   40..72   > > > > pattern path        (@12)
@12   40..72   > > > > > node pattern      (@13:@14 {@15})
@13   41..48   > > > > > > identifier      `person2`
@14   48..55   > > > > > > label           :`Person`
@15   56..71   > > > > > > map             {@16:@17}
@16   57..59   > > > > > > > prop name     `id`
@17   60..70   > > > > > > > parameter     $`person2Id`
@18   73..139  > > MATCH                   OPTIONAL, pattern=@19
@19   88..138  > > > pattern               paths=[@20]
@20   88..138  > > > > named path          @21 = @22
@21   88..92   > > > > > identifier        `path`
@22   95..138  > > > > > shortestPath      single=true, path=@23
@23  108..137  > > > > > > pattern path    (@24)-[@26]-(@29)
@24  108..117  > > > > > > > node pattern  (@25)
@25  109..116  > > > > > > > > identifier  `person1`
@26  117..128  > > > > > > > rel pattern   -[:@27*@28]-
@27  119..125  > > > > > > > > rel type    :`KNOWS`
@28  126..126  > > > > > > > > range       *
@29  128..137  > > > > > > > node pattern  (@30)
@30  129..136  > > > > > > > > identifier  `person2`
@31  139..229  > > RETURN                  projections=[@32]
@32  146..229  > > > projection            expression=@33, alias=@42
@33  146..207  > > > > case                expression=@34, alternatives=[(@36:@37)], default=@39
@34  151..166  > > > > > unary operator    IS NULL @35
@35  151..155  > > > > > > identifier      `path`
@36  171..175  > > > > > TRUE
@37  181..186  > > > > > unary operator    - @38
@38  182..183  > > > > > > integer         1
@39  191..203  > > > > > apply             @40(@41)
@40  191..197  > > > > > > function name   `length`
@41  198..202  > > > > > > identifier      `path`
@42  211..229  > > > > identifier          `shortestPathLength`
