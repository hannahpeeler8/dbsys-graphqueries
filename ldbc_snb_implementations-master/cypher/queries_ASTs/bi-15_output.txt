  @0    2..22   line_comment             // Q15. Social normals
  @1   25..56   block_comment            /*\n  :param { country: 'Burma' }\n*/
  @2   59..925  statement                body=@3
  @3   59..925  > query                  clauses=[@4, @13, @27, @50, @60, @68, @76, @90, @108, @123]
  @4   59..102  > > MATCH                pattern=@5
  @5   67..101  > > > pattern            paths=[@6]
  @6   67..101  > > > > pattern path     (@7)
  @7   67..101  > > > > > node pattern   (@8:@9 {@10})
  @8   68..75   > > > > > > identifier   `country`
  @9   75..83   > > > > > > label        :`Country`
 @10   84..100  > > > > > > map          {@11:@12}
 @11   85..89   > > > > > > > prop name  `name`
 @12   91..99   > > > > > > > parameter  $`country`
 @13  102..178  > > MATCH                pattern=@14
 @14  110..177  > > > pattern            paths=[@15]
 @15  110..177  > > > > pattern path     (@16)-[@18]-(@20)-[@22]-(@24)
 @16  110..119  > > > > > node pattern   (@17)
 @17  111..118  > > > > > > identifier   `country`
 @18  119..135  > > > > > rel pattern    <-[:@19]-
 @19  122..133  > > > > > > rel type     :`IS_PART_OF`
 @20  135..142  > > > > > node pattern   (:@21)
 @21  136..141  > > > > > > label        :`City`
 @22  142..161  > > > > > rel pattern    <-[:@23]-
 @23  145..159  > > > > > > rel type     :`IS_LOCATED_IN`
 @24  161..177  > > > > > node pattern   (@25:@26)
 @25  162..169  > > > > > > identifier   `person1`
 @26  169..176  > > > > > > label        :`Person`
 @27  178..400  > > MATCH                OPTIONAL, pattern=@30
 @28  197..253  > > > line_comment       // start a new MATCH as friend might live in the same City
 @29  258..297  > > > line_comment       // and thus can reuse the IS_PART_OF edge
 @30  300..399  > > > pattern            paths=[@31, @43]
 @31  300..367  > > > > pattern path     (@32)-[@34]-(@36)-[@38]-(@40)
 @32  300..309  > > > > > node pattern   (@33)
 @33  301..308  > > > > > > identifier   `country`
 @34  309..325  > > > > > rel pattern    <-[:@35]-
 @35  312..323  > > > > > > rel type     :`IS_PART_OF`
 @36  325..332  > > > > > node pattern   (:@37)
 @37  326..331  > > > > > > label        :`City`
 @38  332..351  > > > > > rel pattern    <-[:@39]-
 @39  335..349  > > > > > > rel type     :`IS_LOCATED_IN`
 @40  351..367  > > > > > node pattern   (@41:@42)
 @41  352..359  > > > > > > identifier   `friend1`
 @42  359..366  > > > > > > label        :`Person`
 @43  371..399  > > > > pattern path     (@44)-[@46]-(@48)
 @44  371..380  > > > > > node pattern   (@45)
 @45  372..379  > > > > > > identifier   `person1`
 @46  380..390  > > > > > rel pattern    -[:@47]-
 @47  382..388  > > > > > > rel type     :`KNOWS`
 @48  390..399  > > > > > node pattern   (@49)
 @49  391..398  > > > > > > identifier   `friend1`
 @50  400..454  > > WITH                 projections=[@51, @53, @55]
 @51  405..412  > > > projection         expression=@52
 @52  405..412  > > > > identifier       `country`
 @53  414..421  > > > projection         expression=@54
 @54  414..421  > > > > identifier       `person1`
 @55  423..454  > > > projection         expression=@56, alias=@59
 @56  423..437  > > > > apply            @57(@58)
 @57  423..428  > > > > > function name  `count`
 @58  429..436  > > > > > identifier     `friend1`
 @59  441..453  > > > > identifier       `friend1Count`
 @60  454..507  > > WITH                 projections=[@61, @63]
 @61  459..466  > > > projection         expression=@62
 @62  459..466  > > > > identifier       `country`
 @63  468..507  > > > projection         expression=@64, alias=@67
 @64  468..485  > > > > apply            @65(@66)
 @65  468..471  > > > > > function name  `avg`
 @66  472..484  > > > > > identifier     `friend1Count`
 @67  489..506  > > > > identifier       `socialNormalFloat`
 @68  507..562  > > WITH                 projections=[@69, @71]
 @69  512..519  > > > projection         expression=@70
 @70  512..519  > > > > identifier       `country`
 @71  521..562  > > > projection         expression=@72, alias=@75
 @72  521..545  > > > > apply            @73(@74)
 @73  521..526  > > > > > function name  `floor`
 @74  527..544  > > > > > identifier     `socialNormalFloat`
 @75  549..561  > > > > identifier       `socialNormal`
 @76  562..638  > > MATCH                pattern=@77
 @77  570..637  > > > pattern            paths=[@78]
 @78  570..637  > > > > pattern path     (@79)-[@81]-(@83)-[@85]-(@87)
 @79  570..579  > > > > > node pattern   (@80)
 @80  571..578  > > > > > > identifier   `country`
 @81  579..595  > > > > > rel pattern    <-[:@82]-
 @82  582..593  > > > > > > rel type     :`IS_PART_OF`
 @83  595..602  > > > > > node pattern   (:@84)
 @84  596..601  > > > > > > label        :`City`
 @85  602..621  > > > > > rel pattern    <-[:@86]-
 @86  605..619  > > > > > > rel type     :`IS_LOCATED_IN`
 @87  621..637  > > > > > node pattern   (@88:@89)
 @88  622..629  > > > > > > identifier   `person2`
 @89  629..636  > > > > > > label        :`Person`
 @90  638..742  > > MATCH                OPTIONAL, pattern=@91
 @91  655..741  > > > pattern            paths=[@92]
 @92  655..741  > > > > pattern path     (@93)-[@95]-(@97)-[@99]-(@101)-[@104]-(@106)
 @93  655..664  > > > > > node pattern   (@94)
 @94  656..663  > > > > > > identifier   `country`
 @95  664..680  > > > > > rel pattern    <-[:@96]-
 @96  667..678  > > > > > > rel type     :`IS_PART_OF`
 @97  680..687  > > > > > node pattern   (:@98)
 @98  681..686  > > > > > > label        :`City`
 @99  687..706  > > > > > rel pattern    <-[:@100]-
@100  690..704  > > > > > > rel type     :`IS_LOCATED_IN`
@101  706..722  > > > > > node pattern   (@102:@103)
@102  707..714  > > > > > > identifier   `friend2`
@103  714..721  > > > > > > label        :`Person`
@104  722..732  > > > > > rel pattern    -[:@105]-
@105  724..730  > > > > > > rel type     :`KNOWS`
@106  732..741  > > > > > node pattern   (@107)
@107  733..740  > > > > > > identifier   `person2`
@108  742..844  > > WITH                 projections=[@109, @111, @113, @118], WHERE=@120
@109  747..754  > > > projection         expression=@110
@110  747..754  > > > > identifier       `country`
@111  756..763  > > > projection         expression=@112
@112  756..763  > > > > identifier       `person2`
@113  765..795  > > > projection         expression=@114, alias=@117
@114  765..779  > > > > apply            @115(@116)
@115  765..770  > > > > > function name  `count`
@116  771..778  > > > > > identifier     `friend2`
@117  783..795  > > > > identifier       `friend2Count`
@118  797..810  > > > projection         expression=@119
@119  797..809  > > > > identifier       `socialNormal`
@120  816..844  > > > binary operator    @121 = @122
@121  816..828  > > > > identifier       `friend2Count`
@122  831..843  > > > > identifier       `socialNormal`
@123  844..925  > > RETURN               projections=[@124, @129], ORDER BY=@132, LIMIT=@137
@124  853..863  > > > projection         expression=@125, alias=@128
@125  853..863  > > > > property         @126.@127
@126  853..860  > > > > > identifier     `person2`
@127  861..863  > > > > > prop name      `id`
@128  853..863  > > > > identifier       `person2.id`
@129  867..889  > > > projection         expression=@130, alias=@131
@130  867..879  > > > > identifier       `friend2Count`
@131  883..888  > > > > identifier       `count`
@132  889..915  > > > ORDER BY           items=[@133]
@133  900..915  > > > > sort item        expression=@134, ASCENDING
@134  900..911  > > > > > property       @135.@136
@135  900..907  > > > > > > identifier   `person2`
@136  908..910  > > > > > > prop name    `id`
@137  921..924  > > > integer            100
