 @0    0..532  statement                body=@1
 @1    0..532  > query                  clauses=[@2, @39, @53, @60, @81]
 @2    0..183  > > MATCH                pattern=@3, where=@35
 @3    8..157  > > > pattern            paths=[@4, @19]
 @4    8..68   > > > > pattern path     (@5)-[@11]-(@16)
 @5    8..38   > > > > > node pattern   (@6:@7 {@8})
 @6    9..15   > > > > > > identifier   `person`
 @7   15..22   > > > > > > label        :`Person`
 @8   23..37   > > > > > > map          {@9:@10}
 @9   24..26   > > > > > > > prop name  `id`
@10   27..36   > > > > > > > parameter  $`personId`
@11   38..53   > > > > > rel pattern    -[:@12*@13]-
@12   40..46   > > > > > > rel type     :`KNOWS`
@13   46..51   > > > > > > range        *@14..@15
@14   47..48   > > > > > > > integer    1
@15   50..51   > > > > > > > integer    2
@16   53..68   > > > > > node pattern   (@17:@18)
@17   54..60   > > > > > > identifier   `friend`
@18   60..67   > > > > > > label        :`Person`
@19   72..157  > > > > pattern path     (@20)-[@22]-(@24)-[@27]-(@29)
@20   72..80   > > > > > node pattern   (@21)
@21   73..79   > > > > > > identifier   `friend`
@22   80..97   > > > > > rel pattern    <-[:@23]-
@23   83..95   > > > > > > rel type     :`HAS_CREATOR`
@24   97..114  > > > > > node pattern   (@25:@26)
@25   98..108  > > > > > > identifier   `friendPost`
@26  108..113  > > > > > > label        :`Post`
@27  114..127  > > > > > rel pattern    -[:@28]->
@28  116..124  > > > > > > rel type     :`HAS_TAG`
@29  127..157  > > > > > node pattern   (@30:@31 {@32})
@30  128..136  > > > > > > identifier   `knownTag`
@31  136..140  > > > > > > label        :`Tag`
@32  141..156  > > > > > > map          {@33:@34}
@33  142..146  > > > > > > > prop name  `name`
@34  147..155  > > > > > > > parameter  $`tagName`
@35  164..183  > > > unary operator     NOT @36
@36  168..181  > > > > binary operator  @37 = @38
@37  168..174  > > > > > identifier     `person`
@38  175..181  > > > > > identifier     `friend`
@39  183..260  > > MATCH                pattern=@40, where=@49
@40  189..229  > > > pattern            paths=[@41]
@41  189..229  > > > > pattern path     (@42)-[@44]-(@46)
@42  189..201  > > > > > node pattern   (@43)
@43  190..200  > > > > > > identifier   `friendPost`
@44  201..214  > > > > > rel pattern    -[:@45]->
@45  203..211  > > > > > > rel type     :`HAS_TAG`
@46  214..229  > > > > > node pattern   (@47:@48)
@47  215..224  > > > > > > identifier   `commonTag`
@48  224..228  > > > > > > label        :`Tag`
@49  236..260  > > > unary operator     NOT @50
@50  240..258  > > > > binary operator  @51 = @52
@51  240..249  > > > > > identifier     `commonTag`
@52  250..258  > > > > > identifier     `knownTag`
@53  260..302  > > WITH                 DISTINCT, projections=[@54, @56, @58]
@54  274..283  > > > projection         expression=@55
@55  274..283  > > > > identifier       `commonTag`
@56  285..293  > > > projection         expression=@57
@57  285..293  > > > > identifier       `knownTag`
@58  295..302  > > > projection         expression=@59
@59  295..301  > > > > identifier       `friend`
@60  302..417  > > MATCH                pattern=@61, where=@74
@61  308..372  > > > pattern            paths=[@62]
@62  308..372  > > > > pattern path     (@63)-[@65]-(@67)-[@70]-(@72)
@63  308..319  > > > > > node pattern   (@64)
@64  309..318  > > > > > > identifier   `commonTag`
@65  319..332  > > > > > rel pattern    <-[:@66]-
@66  322..330  > > > > > > rel type     :`HAS_TAG`
@67  332..349  > > > > > node pattern   (@68:@69)
@68  333..343  > > > > > > identifier   `commonPost`
@69  343..348  > > > > > > label        :`Post`
@70  349..362  > > > > > rel pattern    -[:@71]->
@71  351..359  > > > > > > rel type     :`HAS_TAG`
@72  362..372  > > > > > node pattern   (@73)
@73  363..371  > > > > > > identifier   `knownTag`
@74  379..416  > > > pattern path       (@75)-[@77]-(@79)
@75  379..391  > > > > node pattern     (@76)
@76  380..390  > > > > > identifier     `commonPost`
@77  391..408  > > > > rel pattern      -[:@78]->
@78  393..405  > > > > > rel type       :`HAS_CREATOR`
@79  408..416  > > > > node pattern     (@80)
@80  409..415  > > > > > identifier     `friend`
@81  417..532  > > RETURN               projections=[@82, @87], ORDER BY=@92, LIMIT=@97
@82  426..451  > > > projection         expression=@83, alias=@86
@83  426..441  > > > > property         @84.@85
@84  426..435  > > > > > identifier     `commonTag`
@85  436..440  > > > > > prop name      `name`
@86  444..451  > > > > identifier       `tagName`
@87  455..486  > > > projection         expression=@88, alias=@91
@88  455..472  > > > > apply            @89(@90)
@89  455..460  > > > > > function name  `count`
@90  461..471  > > > > > identifier     `commonPost`
@91  476..485  > > > > identifier       `postCount`
@92  486..523  > > > ORDER BY           items=[@93, @95]
@93  495..509  > > > > sort item        expression=@94, DESCENDING
@94  495..504  > > > > > identifier     `postCount`
@95  511..523  > > > > sort item        expression=@96, ASCENDING
@96  511..518  > > > > > identifier     `tagName`
@97  529..531  > > > integer            10
