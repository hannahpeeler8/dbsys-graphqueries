/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qgcypher;

/**
 *
 * @author hannah
 */
import java.io.IOException;
import java.util.ArrayList; 
import java.util.HashMap;
import java.util.LinkedList; 
import java.util.List; 
import java.util.Map;
  
class Graph { 
      
    private int size; 
    private List<Node> nodes;
    private List<List<Link>> links; 
    
    public List<Matching> matchings;
    public Map<String, String> map;
    
    public Graph() {
        
    }
  
    public Graph(List<Matching> matchings, Map<String, String> map) throws IOException { 
        this.size = map.size(); 
        nodes = new ArrayList<>(map.size());
        links = new ArrayList<>(map.size()); 
        
        this.matchings = new ArrayList<Matching>();
        for (Matching match : matchings) {
            Matching new_match = new Matching(match.file, match.start, match.conn, match.end);
            this.matchings.add(new_match);
        }
        this.map = new HashMap<String, String>();
        
        //System.out.println("Nodes: ");
        for (Map.Entry<String,String> entry : map.entrySet()) {
            this.map.put(entry.getKey(), entry.getValue());
            nodes.add(new Node(entry.getKey(), entry.getValue()));
            //System.out.println(entry.getKey() + ":" + entry.getValue());
        } 
          
        for (int i = 0; i < map.size(); i++)  {
            links.add(new LinkedList<>()); 
        }
        
        for (Matching match : matchings) {
            Node endNode = null;
            Node startNode = null;
            for (Node node : nodes) {
                if (match.endString.contains(":")) {
                    if (node.getIdentifier().equals(match.endString.substring(0, match.endString.indexOf(":")))) {
                        endNode = node;
                    }   
                }
                else if (node.getIdentifier().equals(match.endString)) {
                    endNode = node;
                }
                
                if (match.startString.contains(":")) {
                    if (node.getIdentifier().equals(match.startString.substring(0, match.startString.indexOf(":")))) {
                        startNode = node;
                    }   
                }
                else if (node.getIdentifier().equals(match.startString)) {
                    startNode = node;
                }
            }
            if (endNode != null && startNode != null) {
                if (match.direct == Direction.LEFT) {
                    addEdge(endNode, startNode, match.connString);
                }
                else if (match.direct == Direction.RIGHT) {
                    addEdge(startNode, endNode, match.connString);
                }
                else {
                    addEdge(endNode, startNode, match.connString);
                    addEdge(startNode, endNode, match.connString);
                }
            }
        }
    } 
    
    // I will only go up to 5 for the purposes of this assignment
    public Integer minimumCycleCutSet() throws IOException {
        
        // check for 0
        if (!this.isCyclic()) {
            return 0;
        }
        if (helper_check1() == 1) {
            return 1;
        }
        if (helper_check2() == 2) {
            return 2;
        }
        if (helper_check3() == 3) {
            return 3;
        }
        if (helper_check4() == 4) {
            return 4;
        }
        if (helper_check5() == 5) {
            return 5;
        }
        return -1;
    }
    
    public Graph deepCopy(Graph old) throws IOException {
        return new Graph(old.matchings, old.map);
    }
      
    private boolean isCyclicUtil(int i, boolean[] visited, boolean[] recStack) { 
          
        // Mark the current node as visited and 
        // part of recursion stack 
        if (recStack[i]) 
            return true; 
  
        if (visited[i]) 
            return false; 
              
        visited[i] = true; 
  
        recStack[i] = true; 
        List<Link> children = links.get(i); 
          
        for (Link c: children) 
            if (isCyclicUtil(nodes.indexOf(c.getNode()), visited, recStack)) 
                return true; 
                  
        recStack[i] = false; 
  
        return false; 
    } 
  
    public void addEdge(Node source, Node dest, String conn) { 
        //System.out.println(source.toString());
        Integer indexSource = nodes.indexOf(source);
        //System.out.println(links.toString());
        //System.out.println(indexSource);
        links.get(indexSource).add(new Link(dest, conn)); 
    } 
  
    // Returns true if the graph contains a  
    // cycle, else false. 
    // This function is a variation of DFS()
    // This is adapted from an implementation by Sagar Shah
    public boolean isCyclic() { 
          
        // Mark all the vertices as not visited and 
        // not part of recursion stack 
        boolean[] visited = new boolean[size]; 
        boolean[] recStack = new boolean[size]; 
          
          
        // Call the recursive helper function to 
        // detect cycle in different DFS trees 
        for (int i = 0; i < size; i++) 
            if (isCyclicUtil(i, visited, recStack)) 
                return true; 
  
        return false; 
    } 

    public Integer helper_check1() throws IOException {
        for (Node node : nodes) {
            Graph curr = deepCopy(this);
            curr.links.remove(nodes.indexOf(node));
            for (List<Link> adj : links) {
                List<Link> links_to_remove = new ArrayList<Link>();
                for (Link link: adj) {
                    if (link.getNode() == node) {
                        links_to_remove.add(link);
                    }
                }
                for (Link link : links_to_remove) {
                    List<Link> curr_adj;
                    if (nodes.indexOf(node) > this.links.indexOf(adj)) {
                        curr_adj = curr.links.get(this.links.indexOf(adj));
                    }
                    else {
                        curr_adj = curr.links.get(this.links.indexOf(adj) - 1);
                    }
                    curr_adj.remove(adj.indexOf(link));
                }
            }
            curr.nodes.remove(this.nodes.indexOf(node));
            curr.map.remove(node.getIdentifier(), node.getLabel());
            curr.size = size - 1;
            if (!curr.isCyclic()) {
                return 1;
            }
        }
        return -1;
    }
    
    public Integer helper_check2() throws IOException {
        for (Node node : nodes) {
            Graph curr = deepCopy(this);
            curr.links.remove(nodes.indexOf(node));
            for (List<Link> adj : links) {
                List<Link> links_to_remove = new ArrayList<Link>();
                for (Link link: adj) {
                    if (link.getNode() == node) {
                        links_to_remove.add(link);
                    }
                }
                for (Link link : links_to_remove) {
                    List<Link> curr_adj;
                    if (nodes.indexOf(node) > this.links.indexOf(adj)) {
                        curr_adj = curr.links.get(this.links.indexOf(adj));
                    }
                    else {
                        curr_adj = curr.links.get(this.links.indexOf(adj) - 1);
                    }
                    curr_adj.remove(adj.indexOf(link));
                }
            }
            curr.nodes.remove(this.nodes.indexOf(node));
            curr.map.remove(node.getIdentifier(), node.getLabel());
            curr.size = curr.size - 1;
            
            
            for (Node node2 : curr.nodes) {
                Graph curr2 = deepCopy(curr);
                curr2.links.remove(curr.nodes.indexOf(node2));
                for (List<Link> adj : curr.links) {
                    List<Link> links_to_remove = new ArrayList<Link>();
                    for (Link link: adj) {
                        if (link.getNode() == node2) {
                            links_to_remove.add(link);
                        }
                    }
                    for (Link link : links_to_remove) {
                        List<Link> curr_adj;
                        if (curr.nodes.indexOf(node2) > curr.links.indexOf(adj)) {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj));
                        }
                        else {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj) - 1);
                        }
                        curr_adj.remove(adj.indexOf(link));
                    }
                }
                curr2.nodes.remove(curr.nodes.indexOf(node2));
                curr2.map.remove(node.getIdentifier(), node.getLabel());
                curr2.size = curr2.size - 1;
                if (!curr2.isCyclic()) {
                    return 2;
                }
            }
        }
        return -1;
    }
    
    public Integer helper_check3() throws IOException {
        for (Node node : nodes) {
            Graph curr = deepCopy(this);
            curr.links.remove(nodes.indexOf(node));
            for (List<Link> adj : links) {
                List<Link> links_to_remove = new ArrayList<Link>();
                for (Link link: adj) {
                    if (link.getNode() == node) {
                        links_to_remove.add(link);
                    }
                }
                for (Link link : links_to_remove) {
                    List<Link> curr_adj;
                    if (nodes.indexOf(node) > this.links.indexOf(adj)) {
                        curr_adj = curr.links.get(this.links.indexOf(adj));
                    }
                    else {
                        curr_adj = curr.links.get(this.links.indexOf(adj) - 1);
                    }
                    curr_adj.remove(adj.indexOf(link));
                }
            }
            curr.nodes.remove(this.nodes.indexOf(node));
            curr.map.remove(node.getIdentifier(), node.getLabel());
            curr.size = curr.size - 1;
            
            
            for (Node node2 : curr.nodes) {
                Graph curr2 = deepCopy(curr);
                curr2.links.remove(curr.nodes.indexOf(node2));
                for (List<Link> adj : curr.links) {
                    List<Link> links_to_remove = new ArrayList<Link>();
                    for (Link link: adj) {
                        if (link.getNode() == node2) {
                            links_to_remove.add(link);
                        }
                    }
                    for (Link link : links_to_remove) {
                        List<Link> curr_adj;
                        if (curr.nodes.indexOf(node2) > curr.links.indexOf(adj)) {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj));
                        }
                        else {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj) - 1);
                        }
                        curr_adj.remove(adj.indexOf(link));
                    }
                }
                curr2.nodes.remove(curr.nodes.indexOf(node2));
                curr2.map.remove(node.getIdentifier(), node.getLabel());
                curr2.size = curr2.size - 1;
                
                for (Node node3 : curr2.nodes) {
                    Graph curr3 = deepCopy(curr2);
                    curr3.links.remove(curr2.nodes.indexOf(node3));
                    for (List<Link> adj : curr2.links) {
                        List<Link> links_to_remove = new ArrayList<Link>();
                        for (Link link: adj) {
                            if (link.getNode() == node3) {
                                links_to_remove.add(link);
                            }
                        }
                        for (Link link : links_to_remove) {
                            List<Link> curr_adj;
                            if (curr2.nodes.indexOf(node3) > curr2.links.indexOf(adj)) {
                                curr_adj = curr3.links.get(curr2.links.indexOf(adj));
                            }
                            else {
                                curr_adj = curr3.links.get(curr2.links.indexOf(adj) - 1);
                            }
                            curr_adj.remove(adj.indexOf(link));
                        }
                    }
                    curr3.nodes.remove(curr2.nodes.indexOf(node3));
                    curr3.map.remove(node.getIdentifier(), node.getLabel());
                    curr3.size = curr3.size - 1;

                    if (!curr3.isCyclic()) {
                        return 3;
                    }
                }
            }
        }
        return -1;
    }
    
    public Integer helper_check4() throws IOException {
        for (Node node : nodes) {
            Graph curr = deepCopy(this);
            curr.links.remove(nodes.indexOf(node));
            for (List<Link> adj : links) {
                List<Link> links_to_remove = new ArrayList<Link>();
                for (Link link: adj) {
                    if (link.getNode() == node) {
                        links_to_remove.add(link);
                    }
                }
                for (Link link : links_to_remove) {
                    List<Link> curr_adj;
                    if (nodes.indexOf(node) > this.links.indexOf(adj)) {
                        curr_adj = curr.links.get(this.links.indexOf(adj));
                    }
                    else {
                        curr_adj = curr.links.get(this.links.indexOf(adj) - 1);
                    }
                    curr_adj.remove(adj.indexOf(link));
                }
            }
            curr.nodes.remove(this.nodes.indexOf(node));
            curr.map.remove(node.getIdentifier(), node.getLabel());
            curr.size = curr.size - 1;
            
            
            for (Node node2 : curr.nodes) {
                Graph curr2 = deepCopy(curr);
                curr2.links.remove(curr.nodes.indexOf(node2));
                for (List<Link> adj : curr.links) {
                    List<Link> links_to_remove = new ArrayList<Link>();
                    for (Link link: adj) {
                        if (link.getNode() == node2) {
                            links_to_remove.add(link);
                        }
                    }
                    for (Link link : links_to_remove) {
                        List<Link> curr_adj;
                        if (curr.nodes.indexOf(node2) > curr.links.indexOf(adj)) {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj));
                        }
                        else {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj) - 1);
                        }
                        curr_adj.remove(adj.indexOf(link));
                    }
                }
                curr2.nodes.remove(curr.nodes.indexOf(node2));
                curr2.map.remove(node.getIdentifier(), node.getLabel());
                curr2.size = curr2.size - 1;
                
                for (Node node3 : curr2.nodes) {
                    Graph curr3 = deepCopy(curr2);
                    curr3.links.remove(curr2.nodes.indexOf(node3));
                    for (List<Link> adj : curr2.links) {
                        List<Link> links_to_remove = new ArrayList<Link>();
                        for (Link link: adj) {
                            if (link.getNode() == node3) {
                                links_to_remove.add(link);
                            }
                        }
                        for (Link link : links_to_remove) {
                            List<Link> curr_adj;
                            if (curr2.nodes.indexOf(node3) > curr2.links.indexOf(adj)) {
                                curr_adj = curr3.links.get(curr2.links.indexOf(adj));
                            }
                            else {
                                curr_adj = curr3.links.get(curr2.links.indexOf(adj) - 1);
                            }
                            curr_adj.remove(adj.indexOf(link));
                        }
                    }
                    curr3.nodes.remove(curr2.nodes.indexOf(node3));
                    curr3.map.remove(node.getIdentifier(), node.getLabel());
                    curr3.size = curr3.size - 1;

                    for (Node node4 : curr3.nodes) {
                        Graph curr4 = deepCopy(curr3);
                        curr4.links.remove(curr3.nodes.indexOf(node4));
                        for (List<Link> adj : curr3.links) {
                            List<Link> links_to_remove = new ArrayList<Link>();
                            for (Link link: adj) {
                                if (link.getNode() == node4) {
                                    links_to_remove.add(link);
                                }
                            }
                            for (Link link : links_to_remove) {
                                List<Link> curr_adj;
                                if (curr3.nodes.indexOf(node4) > curr3.links.indexOf(adj)) {
                                    curr_adj = curr4.links.get(curr3.links.indexOf(adj));
                                }
                                else {
                                    curr_adj = curr4.links.get(curr3.links.indexOf(adj) - 1);
                                }
                                curr_adj.remove(adj.indexOf(link));
                            }
                        }
                        curr4.nodes.remove(curr3.nodes.indexOf(node4));
                        curr4.map.remove(node.getIdentifier(), node.getLabel());
                        curr4.size = curr4.size - 1;

                        if (!curr4.isCyclic()) {
                            return 4;
                        }
                    }
                }
            }
        }
        return -1;
    }
    
    public Integer helper_check5() throws IOException {
        for (Node node : nodes) {
            Graph curr = deepCopy(this);
            curr.links.remove(nodes.indexOf(node));
            for (List<Link> adj : links) {
                List<Link> links_to_remove = new ArrayList<Link>();
                for (Link link: adj) {
                    if (link.getNode() == node) {
                        links_to_remove.add(link);
                    }
                }
                for (Link link : links_to_remove) {
                    List<Link> curr_adj;
                    if (nodes.indexOf(node) > this.links.indexOf(adj)) {
                        curr_adj = curr.links.get(this.links.indexOf(adj));
                    }
                    else {
                        curr_adj = curr.links.get(this.links.indexOf(adj) - 1);
                    }
                    curr_adj.remove(adj.indexOf(link));
                }
            }
            curr.nodes.remove(this.nodes.indexOf(node));
            curr.map.remove(node.getIdentifier(), node.getLabel());
            curr.size = curr.size - 1;
            
            
            for (Node node2 : curr.nodes) {
                Graph curr2 = deepCopy(curr);
                curr2.links.remove(curr.nodes.indexOf(node2));
                for (List<Link> adj : curr.links) {
                    List<Link> links_to_remove = new ArrayList<Link>();
                    for (Link link: adj) {
                        if (link.getNode() == node2) {
                            links_to_remove.add(link);
                        }
                    }
                    for (Link link : links_to_remove) {
                        List<Link> curr_adj;
                        if (curr.nodes.indexOf(node2) > curr.links.indexOf(adj)) {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj));
                        }
                        else {
                            curr_adj = curr2.links.get(curr.links.indexOf(adj) - 1);
                        }
                        curr_adj.remove(adj.indexOf(link));
                    }
                }
                curr2.nodes.remove(curr.nodes.indexOf(node2));
                curr2.map.remove(node.getIdentifier(), node.getLabel());
                curr2.size = curr2.size - 1;
                
                for (Node node3 : curr2.nodes) {
                    Graph curr3 = deepCopy(curr2);
                    curr3.links.remove(curr2.nodes.indexOf(node3));
                    for (List<Link> adj : curr2.links) {
                        List<Link> links_to_remove = new ArrayList<Link>();
                        for (Link link: adj) {
                            if (link.getNode() == node3) {
                                links_to_remove.add(link);
                            }
                        }
                        for (Link link : links_to_remove) {
                            List<Link> curr_adj;
                            if (curr2.nodes.indexOf(node3) > curr2.links.indexOf(adj)) {
                                curr_adj = curr3.links.get(curr2.links.indexOf(adj));
                            }
                            else {
                                curr_adj = curr3.links.get(curr2.links.indexOf(adj) - 1);
                            }
                            curr_adj.remove(adj.indexOf(link));
                        }
                    }
                    curr3.nodes.remove(curr2.nodes.indexOf(node3));
                    curr3.map.remove(node.getIdentifier(), node.getLabel());
                    curr3.size = curr3.size - 1;

                    for (Node node4 : curr3.nodes) {
                        Graph curr4 = deepCopy(curr3);
                        curr4.links.remove(curr3.nodes.indexOf(node4));
                        for (List<Link> adj : curr3.links) {
                            List<Link> links_to_remove = new ArrayList<Link>();
                            for (Link link: adj) {
                                if (link.getNode() == node4) {
                                    links_to_remove.add(link);
                                }
                            }
                            for (Link link : links_to_remove) {
                                List<Link> curr_adj;
                                if (curr3.nodes.indexOf(node4) > curr3.links.indexOf(adj)) {
                                    curr_adj = curr4.links.get(curr3.links.indexOf(adj));
                                }
                                else {
                                    curr_adj = curr4.links.get(curr3.links.indexOf(adj) - 1);
                                }
                                curr_adj.remove(adj.indexOf(link));
                            }
                        }
                        curr4.nodes.remove(curr3.nodes.indexOf(node4));
                        curr4.map.remove(node.getIdentifier(), node.getLabel());
                        curr4.size = curr4.size - 1;

                        for (Node node5 : curr4.nodes) {
                            Graph curr5 = deepCopy(curr4);
                            curr5.links.remove(curr4.nodes.indexOf(node5));
                            for (List<Link> adj : curr4.links) {
                                List<Link> links_to_remove = new ArrayList<Link>();
                                for (Link link: adj) {
                                    if (link.getNode() == node5) {
                                        links_to_remove.add(link);
                                    }
                                }
                                for (Link link : links_to_remove) {
                                    List<Link> curr_adj;
                                    if (curr4.nodes.indexOf(node5) > curr4.links.indexOf(adj)) {
                                        curr_adj = curr5.links.get(curr4.links.indexOf(adj));
                                    }
                                    else {
                                        curr_adj = curr5.links.get(curr4.links.indexOf(adj) - 1);
                                    }
                                    curr_adj.remove(adj.indexOf(link));
                                }
                            }
                            curr5.nodes.remove(curr4.nodes.indexOf(node5));
                            curr5.map.remove(node.getIdentifier(), node.getLabel());
                            curr5.size = curr5.size - 1;

                            if (!curr5.isCyclic()) {
                                return 5;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }
    
} 
