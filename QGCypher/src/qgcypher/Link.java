/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qgcypher;

/**
 *
 * @author hannah
 */
public class Link {
    
    public Node node;
    public String relationship;
    
    public Link(Node node, String conn) {
        this.node = node;
        this.relationship = conn;
    }
    
    public Node getNode() {
        return node;
    }
    
    public String getConn() {
        return relationship;
    }
    
}
