/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qgcypher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hannah
 */
public class QGCypher {
    
    // set filepath to the abstract syntax tree you are using
    static String filepath_base = "/home/hannah/Documents/Spring2019/"
                    + "DB/TermProject_HannahPeeler/ldbc_snb_implementations-master/"
                    + "cypher/queries_ASTs/";

    static String[] queries = new String[56];

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            
            queries[0] = "mytest_output.txt";
            queries[1] = "bi-1_output.txt";
            queries[2] = "bi-2_output.txt";
            queries[3] = "bi-3_output.txt";
            queries[4] = "bi-4_output.txt";
            queries[5] = "bi-5_output.txt";
            queries[6] = "bi-6_output.txt";
            queries[7] = "bi-7_output.txt";
            queries[8] = "bi-8_output.txt";
            queries[9] = "bi-9_output.txt";
            queries[10] = "bi-10_output.txt";
            queries[11] = "bi-11_output.txt";
            queries[12] = "bi-12_output.txt";
            queries[13] = "bi-13_output.txt";
            queries[14] = "bi-14_output.txt";
            queries[15] = "bi-15_output.txt";
            queries[16] = "bi-17_output.txt";
            queries[17] = "bi-18_output.txt";
            queries[18] = "bi-19_output.txt";
            queries[19] = "bi-20_output.txt";
            queries[20] = "bi-21_output.txt";
            //queries[21] = "bi-22_output.txt";
            queries[22] = "bi-23_output.txt";
            queries[23] = "bi-24_output.txt";
            //queries[24] = "bi-25_output.txt";
            //queries[25] = "bi-10-without-pattern-comprehension_output.txt";
            queries[26] = "interactive-complex-1_output.txt";
            queries[27] = "interactive-complex-2_output.txt";
            queries[28] = "interactive-complex-3_output.txt";
            //queries[29] = "interactive-complex-4_output.txt";
            queries[30] = "interactive-complex-5_output.txt";
            queries[31] = "interactive-complex-6_output.txt";
            queries[32] = "interactive-complex-7_output.txt";
            queries[33] = "interactive-complex-8_output.txt";
            queries[34] = "interactive-complex-9_output.txt";
            queries[35] = "interactive-complex-10_output.txt";
            queries[36] = "interactive-complex-11_output.txt";
            queries[37] = "interactive-complex-12_output.txt";
            //queries[38] = "interactive-complex-13_output.txt";
            //queries[39] = "interactive-complex-14_output.txt";
            queries[40] = "interactive-short-1_output.txt";
            queries[41] = "interactive-short-2_output.txt";
            queries[42] = "interactive-short-3_output.txt";
            queries[43] = "interactive-short-4_output.txt";
            queries[44] = "interactive-short-5_output.txt";
            queries[45] = "interactive-short-6_output.txt";
            queries[46] = "interactive-short-7_output.txt";
            queries[47] = "interactive-update-1_output.txt";
            queries[48] = "interactive-update-2_output.txt";
            queries[49] = "interactive-update-3_output.txt";
            queries[50] = "interactive-update-4_output.txt";
            queries[51] = "interactive-update-5_output.txt";
            queries[52] = "interactive-update-6_output.txt";
            queries[53] = "interactive-update-7_output.txt";
            queries[54] = "interactive-update-8_output.txt";
        
            for (String query : queries) {
                
                Integer print_num = 0;
                if (query != null) {
                    
                    String filepath = filepath_base + query;
                    File file = new File(filepath);

                    String curr_line = null;

                    // scan until we find the list of clauses
                    final Scanner s = new Scanner(file);
                    while(s.hasNextLine()) {
                        curr_line = s.nextLine();
                        if (curr_line.contains("clauses=")) {
                            break;
                        }
                    }

                    List<Integer> clauses = new ArrayList<Integer>();
                    getArrayOfLineNums(curr_line, clauses);

                    List<Matching> matchings = new ArrayList<Matching>();

                    //System.out.println("Paths:");
                    for (int i = 0; i < clauses.size(); i++) {

                        curr_line = Files.readAllLines(Paths.get(filepath)).get(clauses.get(i));

                        // for the purposes of this application, only 

                        if (curr_line.contains("MATCH")) {

                            List<Integer> pattern_where = new ArrayList<Integer>();
                            getPatternAndWhere(curr_line, pattern_where);

                            curr_line = Files.readAllLines(Paths.get(filepath)).get(pattern_where.get(0));

                            List<Integer> patternPaths = new ArrayList<Integer>();
                            getArrayOfLineNums(curr_line, patternPaths);               
                            //System.out.println(patternPaths.toString());

                            Integer start = null;
                            Integer conn = null;
                            Integer end = null;
                            Boolean first_pass = true;


                            for (Integer path : patternPaths) {
                                curr_line = Files.readAllLines(Paths.get(filepath)).get(path);
                                //System.out.println(curr_line);
                                if (!curr_line.substring(curr_line.indexOf("@") + 1, curr_line.lastIndexOf("@")).contains("@")) {
                                    //do nothing
                                }
                                else {
                                    while(curr_line.contains("@")) {

                                        if (first_pass) {
                                            curr_line = curr_line.substring(curr_line.indexOf("@") + 1, curr_line.length() - 1);
                                            end = Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1));
                                            curr_line = curr_line.substring(0, curr_line.lastIndexOf("@") - 3);
                                            first_pass = false;
                                        }
                                        else {
                                            end = start;
                                        }


                                        conn = Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1));
                                        curr_line = curr_line.substring(0, curr_line.lastIndexOf("@") - 3);

                                        start = Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1));
                                        curr_line = curr_line.substring(0, curr_line.lastIndexOf("@") - 3);

                                        matchings.add(new Matching(filepath, start, conn, end));

                                    }
                                }
                                first_pass = true;

                            }
                            for(Matching m : matchings) {
                                print_num++;
                                //System.out.println(m.toString());
                                //System.out.println(m.queryGraph());
                            }

                        }
                    }

                    Map<String, String> map = Matching.getIdentifierMap();
                    Graph graph = new Graph(matchings, map);
                    //System.out.println(graph.isCyclic());
                    System.out.println(map.size() + ", " + print_num + ", " + matchings.size());
                    System.out.println("MinimumCycleCutSet for " + query + ": " + graph.minimumCycleCutSet());
                    
                }
                
            }
            
            
        } catch (IOException ex) {
            Logger.getLogger(QGCypher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void getArrayOfLineNums(String curr_line, List<Integer> clauses) {
        curr_line = curr_line.substring(curr_line.indexOf("@") + 1, curr_line.length() - 1);
        while(curr_line.contains("@")) {
            clauses.add(0, Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1)));
            curr_line = curr_line.substring(0, curr_line.lastIndexOf("@"));
            if (curr_line.contains("@")) {
                curr_line = curr_line.substring(0, curr_line.lastIndexOf(","));
            }
        }
    }
    
    public static void getPatternAndWhere(String curr_line, List<Integer> PatternAndWhere) {
        if (curr_line.contains("where=")) {
            PatternAndWhere.add(0, Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@") + 1)));
            curr_line = curr_line.substring(0, curr_line.lastIndexOf(","));
        }
        else {
            PatternAndWhere.add(0, -1);
        }
        PatternAndWhere.add(0, Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@") + 1)));

    }
    
}
