/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qgcypher;

/**
 *
 * @author hannah
 */
public class Node {
    
    String identifier;
    String label;
    
    public Node(String i, String l) {
        this.identifier = i;
        this.label = l;
    }
    
    public String getIdentifier() {
        return identifier;
    }
    
    public String getLabel() {
        return label;
    }
    
    public String toString() {
        return getIdentifier() + ":" + getLabel();
    }
    
}
