/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qgcypher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * @author hannah
 */

enum Direction 
{ 
    LEFT, RIGHT, BI; 
} 

public class Matching {
    
    Integer start;
    String startString;
    Integer conn;
    String connString;
    Integer end;
    String endString;
    Direction direct;
    String file;
    
    public static String junk = "junk1";
    public static Integer junk_num = 1;
    
    public static Map<String, String> known_identifiers = new HashMap<String, String>();
    
    public Matching (String filepath, Integer start, Integer conn, Integer end) throws IOException {
        //System.out.println("Within contrsuctor");
        this.start = start;
        this.conn = conn;
        this.end = end;
        this.file = filepath;
        
        //get startString
        this.startString = getStartEndString(filepath, start);
        
        // get connString and direction
        this.connString = getConnString(filepath, conn);
        this.direct = getDirection(filepath, conn);
        
        //get startString
        this.endString = getStartEndString(filepath, end);
        
    }
    
    public static Map<String, String> getIdentifierMap() {
        return known_identifiers;
    }
    
    public static Direction getDirection(String filepath, Integer conn) throws IOException {
        String curr_line = Files.readAllLines(Paths.get(filepath)).get(conn);
        if (curr_line.contains("->")) {
            return Direction.RIGHT;
        }
        else if (curr_line.contains("<-")) {
            return Direction.LEFT;
        }
        else {
            return Direction.BI;
        }
    }
    
    public static String getConnString(String filepath, Integer conn) throws IOException {
        String curr_line = Files.readAllLines(Paths.get(filepath)).get(conn);
        if (curr_line.contains("->")) {
            curr_line = curr_line.substring(curr_line.indexOf("@") + 1, curr_line.length() - 3);
        }
        else if (curr_line.contains("<-")) {
            curr_line = curr_line.substring(curr_line.indexOf("@") + 1, curr_line.length() - 2);
        }
        else {
            curr_line = curr_line.substring(curr_line.indexOf("@") + 1, curr_line.length() - 2);
        }

        Integer connNum = null;

        while (curr_line.contains("@") && curr_line.contains(":")) {
            //System.out.println(curr_line);
            connNum = Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1));
            curr_line = curr_line.substring(0, curr_line.lastIndexOf("@") - 1);
        }
        //System.out.println(connNum);
        curr_line = Files.readAllLines(Paths.get(filepath)).get(connNum);
        return curr_line.substring(curr_line.indexOf("`"), curr_line.length());
    }
    
    public static String getStartEndString(String filepath, Integer startend) throws IOException {
        String curr_line = Files.readAllLines(Paths.get(filepath)).get(startend);
        curr_line = trimString(curr_line);
        
        if (!curr_line.contains("(:") && curr_line.contains(":")) {
            //System.out.println(curr_line);
            // get startString
            Integer labelNum = Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1));
            curr_line = curr_line.substring(0, curr_line.lastIndexOf("@") - 1);
            Integer identifierNum = Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1));
            //System.out.println("IdentifierNum: " + identifierNum + ", and LabelNum: " + labelNum);
            
            curr_line = Files.readAllLines(Paths.get(filepath)).get(identifierNum);
            String identifier = curr_line.substring(curr_line.indexOf("`"), curr_line.length());
            curr_line = Files.readAllLines(Paths.get(filepath)).get(labelNum);
            String label = curr_line.substring(curr_line.indexOf("`"), curr_line.length());
            known_identifiers.put(identifier, label);
            return identifier + ":" + label;
            
        }
        else {
            
            Integer identifierNum = Integer.parseInt(curr_line.substring(curr_line.lastIndexOf("@")+1));
            //System.out.println(identifierNum);
            
            curr_line = Files.readAllLines(Paths.get(filepath)).get(identifierNum);
            String identifier = curr_line.substring(curr_line.indexOf("`"), curr_line.length());
            
            if (known_identifiers.containsKey(identifier)) {
                return identifier + ":" + known_identifiers.get(identifier);
            }
            else {
                return identifier;
            }
            
        }
    }
    
    public static String trimString(String curr_line) {
        if (curr_line.contains("{")) {
            curr_line = curr_line.substring(curr_line.indexOf("@") + 1, curr_line.indexOf("{") - 1);
        }
        else {
            curr_line = curr_line.substring(curr_line.indexOf("@") + 1, curr_line.length() - 1);
        }
        return curr_line;
    }
    
    public String toString() {
        return "(@" + start + ")-[@" + conn + "]-(@" + end + ")";
    }
    
    public String queryGraph() {
        if (this.direct == Direction.LEFT) {
            return startString + " <- [" + connString + "] - " + endString;
        }
        else if (this.direct == Direction.RIGHT) {
            return startString + " - [" + connString + "] -> " + endString;
        }
        else {
            return startString + " <- [" + connString + "] -> " + endString;
        }
    }
    
}
